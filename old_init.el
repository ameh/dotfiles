;; Packages -- 

(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/") t)
(add-to-list 'package-archives '("melpa-stable" . "http://stable.melpa.org/packages/") t)
(package-initialize)

;;BACKUPS
(setq backup-directory-alist `(("." . "~/.saves")))
(setq create-lockfiles nil)


;; Daemon
(defun startup_func(_)
  (load-file "/home/amey/.emacs.d/init.el")
  (menu-bar-mode 0)
  (toggle-scroll-bar 0)
  (tool-bar-mode 0))
  ;;(set-scroll-bar-mode 0))

(add-hook 'after-make-frame-functions #'startup_func)
;;

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(eval-when-compile
  (require 'use-package))
(setq-default use-package-always-ensure t)

;; GUI -- 

(when (display-graphic-p)
  (menu-bar-mode 0)
  (toggle-scroll-bar 0)
  (tool-bar-mode 0))

;; Theme --

(require 'gruvbox-theme)


;; Fonts --

(set-frame-font "Inconsolata 12" nil t)

;; Modeline --

(line-number-mode t)
(column-number-mode t)

;; Splash Screen

(use-package dashboard
  :custom
  (dashboard-banner-logo-title
   (format "[Emacs ready in %.2f seconds with %d garbage collections.]"
           (float-time (time-subtract after-init-time before-init-time)) gcs-done))
  (dashboard-startup-banner 'logo)
  :config
  (dashboard-setup-startup-hook))

;; Lines

;;(setq-default transient-mark-mode t
;;visual-line-mode t
;;indent-tabs-mode nil
;;tab-width 4)

(when (display-graphic-p)
  (global-hl-line-mode t))

(use-package linum
  :hook (prog-mode . linum-mode)
  :custom
  (linum-format " %d ")
  :config
  (set-face-underline 'linum nil))

;; Scrolling

(setq-default scroll-margin 0
              scroll-conservatively 10000
              scroll-preserve-screen-position t
              mouse-wheel-progressive-speed nil)

;; Completion

(use-package ivy
  :bind
  ("C-x b" . ivy-switch-buffer)
  (:map ivy-minibuffer-map
   ("<return>" . ivy-alt-done))
  :custom
  (ivy-use-virtual-buffers t)
  (ivy-count-format "%d/%d ")
  (ivy-height 20)
  (ivy-display-style 'fancy)
  (ivy-format-function 'ivy-format-function-line)
  (ivy-re-builders-alist
      '((t . ivy--regex-plus)))
  (ivy-initial-inputs-alist nil))

(use-package counsel
  :bind
  (("M-x" . counsel-M-x)
   ("C-x C-f" . counsel-find-file)
   ("C-h v" . counsel-describe-variable)
   ("C-h f" . counsel-describe-function)))

(use-package swiper
  :bind
  ("C-s" . swiper))

;; Programming

(use-package aggressive-indent
  :config
  (global-aggressive-indent-mode 0))

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package smartparens
  :hook (prog-mode . smartparens-mode)
  :custom
  (sp-escape-quotes-after-insert nil)
  :config
  (require 'smartparens-config))

(show-paren-mode t)

;; GIT

(use-package magit
  :bind
  ("C-c g" . magit-status))

(use-package gitignore-mode
  :mode ("\\.gitignore\\'" . gitignore-mode))

;; Auto Complete

(use-package company
  :bind
  ("M-/" . company-complete)
  (:map company-active-map
   ("M-n" . nil)
   ("M-p" . nil)
   ("C-n" . company-select-next)
   ("C-p" . company-select-previous))
  :custom-face
  (company-tooltip ((t (:foreground "#ABB2BF" :background "#30343C"))))
  (company-tooltip-annotation ((t (:foreground "#ABB2BF" :background "#30343C"))))
  (company-tooltip-selection ((t (:foreground "#ABB2BF" :background "#393F49"))))
  (company-tooltip-mouse ((t (:background "#30343C"))))
  (company-tooltip-common ((t (:foreground "#ABB2BF" :background "#30343C"))))
  (company-tooltip-common-selection ((t (:foreground "#ABB2BF" :background "#393F49"))))
  (company-preview ((t (:background "#30343C"))))
  (company-preview-common ((t (:foreground "#ABB2BF" :background "#30343C"))))
  (company-scrollbar-fg ((t (:background "#30343C"))))
  (company-scrollbar-bg ((t (:background "#30343C"))))
  (company-template-field ((t (:foreground "#282C34" :background "#C678DD"))))
  :custom
  (company-require-match 'never)
  (company-dabbrev-downcase nil)
  (company-tooltip-align-annotations t)
  (company-idle-delay 128)
  (company-minimum-prefix-length 128)
  :config
  (global-company-mode t))

(use-package company-quickhelp
  :if (display-graphic-p)
  :after company
  (company-quickhelp-mode))

;; Snips

(use-package yasnippet
  :hook (prog-mode . yas-minor-mode)
  :config
  (use-package yasnippet-snippets))

;; Lint

(use-package flycheck
  :custom-face
  (flycheck-info ((t (:underline (:style line :color "#80FF80")))))
  (flycheck-warning ((t (:underline (:style line :color "#FF9933")))))
  (flycheck-error ((t (:underline (:style line :color "#FF5C33")))))
  :custom
  (flycheck-check-syntax-automatically '(mode-enabled save))
  :config
  (define-fringe-bitmap 'flycheck-fringe-bitmap-ball
    (vector #b00000000
	    #b00000000
	    #b00000000
	    #b00000000
	    #b00000000
	    #b00111000
	    #b01111100
	    #b11111110
	    #b11111110
	    #b11111110
	    #b01111100
	    #b00111000
	    #b00000000
	    #b00000000
	    #b00000000
	    #b00000000
	    #b00000000))
  (flycheck-define-error-level 'info
    :severity 100
    :compilation-level 2
    :overlay-category 'flycheck-info-overlay
    :fringe-bitmap 'flycheck-fringe-bitmap-ball
    :fringe-face 'flycheck-fringe-info
    :info-list-face 'flycheck-error-list-info)
  (flycheck-define-error-level 'warning
    :severity 100
    :compilation-level 2
    :overlay-category 'flycheck-warning-overlay
    :fringe-bitmap 'flycheck-fringe-bitmap-ball
    :fringe-face 'flycheck-fringe-warning
    :warning-list-face 'flycheck-error-list-warning)
  (flycheck-define-error-level 'error
    :severity 100
    :compilation-level 2
    :overlay-category 'flycheck-error-overlay
    :fringe-bitmap 'flycheck-fringe-bitmap-ball
    :fringe-face 'flycheck-fringe-error
    :error-list-face 'flycheck-error-list-error)
  (global-flycheck-mode t))

;; Project

(use-package projectile
  :demand t
  :bind
  (:map projectile-mode-map
   ("C-c p" . projectile-command-map))
  :custom
  (projectile-project-search-path '("~/src"))
  (projectile-indexing-method 'hybrid)
  (projectile-sort-order 'access-time)
  (projectile-enable-caching t)
  (projectile-require-project-root t)
  (projectile-completion-system 'ivy)
  :config
  (projectile-mode t)
  (counsel-projectile-mode))

(use-package counsel-projectile
  :after (counsel projectile))

;; Python

(use-package pip-requirements)

(use-package python
  :after flycheck
  :ensure nil
  :hook (python-mode . elpy-mode)
  :custom
  (python-indent 4)
  (python-shell-interpreter (if (memq window-system '(mac ns)) "ipython" "ipython3"))
  (python-shell-interpreter-args "--simple-prompt -i")
  (gud-pdb-command-name "python3 -m pdb")
  (python-fill-docstring-style 'pep-257)
  (py-split-window-on-execute t)
  (flycheck-python-pylint-executable "python3")
  (flycheck-python-pycompile-executable "python3"))

(use-package elpy
  :demand t
  :preface
  (defun python-before-save-hook ()
    (when (eq major-mode 'python-mode)
      (elpy-black-fix-code)))
  :after company
  :custom
  (elpy-rpc-python-command "python3")
  :config
  (delete 'elpy-module-highlight-indentation elpy-modules)
  (delete 'elpy-module-flymake elpy-modules)
  (delete 'elpy-module-company elpy-modules)
  (add-to-list 'company-backends #'elpy-company-backend)
  (add-hook 'before-save-hook #'python-before-save-hook)
  (elpy-enable))

;; C

(use-package cc-mode
  :ensure nil
  :preface
  (define-auto-insert
    (cons "\\.\\([Hh]\\|hh\\|hpp\\)\\'" "C/C++ header")
    '(nil
      (let* ((noext (substring buffer-file-name 0 (match-beginning 0)))
             (nopath (file-name-nondirectory noext))
             (ident (concat (upcase nopath) "_H")))
        (concat "#ifndef " ident "\n"
                "# define " ident  "\n\n"
                "#endif\n"))))

  :hook
  (c-mode . (lambda () (setq indent-tabs-mode t)
                       (global-aggressive-indent-mode -1)))
  :custom
  (c-default-style "linux")
  (c-basic-offset 4))

(use-package srefactor
  :bind
  (:map c-mode-base-map
   ("C-c C-r" . srefactor-refactor-at-point))
  :hook ((c-mode c++-mode) . semantic-mode))

(use-package company-c-headers
  :demand t
  :after company
  :config
  (add-to-list 'company-backends 'company-c-headers))

;; Emacs Custom Garbage -- IGNORE --

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("1436d643b98844555d56c59c74004eb158dc85fc55d2e7205f8d9b8c860e177f" default)))
 '(package-selected-packages
   (quote
    (fstar-mode gruvbox-theme company-c-headers srefactor elpy pip-requirements counsel-projectile projectile yasnippet-snippets flycheck yasnippet company-quickhelp company gitignore-mode magit smartparens rainbow-delimiters aggressive-indent counsel ivy dashboard gotham-theme use-package))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((((class color) (min-colors 16777215)) (:background "#282828" :foreground "#fdf4c1")) (((class color) (min-colors 255)) (:background "#262626" :foreground "#ffffaf"))))
 '(company-preview ((t (:background "#30343C"))))
 '(company-preview-common ((t (:foreground "#ABB2BF" :background "#30343C"))))
 '(company-scrollbar-bg ((t (:background "#30343C"))))
 '(company-scrollbar-fg ((t (:background "#30343C"))))
 '(company-template-field ((t (:foreground "#282C34" :background "#C678DD"))))
 '(company-tooltip ((t (:foreground "#ABB2BF" :background "#30343C"))))
 '(company-tooltip-annotation ((t (:foreground "#ABB2BF" :background "#30343C"))))
 '(company-tooltip-common ((t (:foreground "#ABB2BF" :background "#30343C"))))
 '(company-tooltip-common-selection ((t (:foreground "#ABB2BF" :background "#393F49"))))
 '(company-tooltip-mouse ((t (:background "#30343C"))))
 '(company-tooltip-selection ((t (:foreground "#ABB2BF" :background "#393F49"))))
 '(flycheck-error ((t (:underline (:style line :color "#FF5C33")))))
 '(flycheck-info ((t (:underline (:style line :color "#80FF80")))))
 '(flycheck-warning ((t (:underline (:style line :color "#FF9933"))))))
