;;; bui-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (directory-file-name (or (file-name-directory #$) (car load-path))))

;;;### (autoloads nil nil ("bui-button.el" "bui-core.el" "bui-entry.el"
;;;;;;  "bui-history.el" "bui-info.el" "bui-list.el" "bui-pkg.el"
;;;;;;  "bui-utils.el" "bui.el") (23821 49997 938238 302000))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; bui-autoloads.el ends here
