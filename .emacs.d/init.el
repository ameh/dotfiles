(require 'package)
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
		    (not (gnutls-available-p))))
       (proto (if no-ssl "http" "https")))
  (when no-ssl
        (warn "\
Your version of Emacs does not support SSL connections,
which is unsafe because it allows man-in-the-middle attacks.
There are two things you can do about this warning:
1. Install an Emacs version that does support SSL and be safe.
2. Remove this warning from your init file so you won't see it again."))
  ;; Comment/uncomment these two lines to enable/disable MELPA and MELPA Stable as desired
  (add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)
  ;;(add-to-list 'package-archives (cons "melpa-stable" (concat proto "://stable.melpa.org/packages/")) t)
  (when (< emacs-major-version 24)
    ;; For important compatibility libraries like cl-lib
    (add-to-list 'package-archives (cons "gnu" (concat proto "://elpa.gnu.org/packages/")))))
(package-initialize)

;; General Configuration

(menu-bar-mode -1)
(tool-bar-mode -1)
(setq create-lockfiles nil)
(setq auto-save-default nil)
(setq backup-directory-alist '(("" . "~/.emacs.d/backup")))
(setq inhibit-startup-message t) 
(global-linum-mode t)


(set-default-font "Hack-16")



;; Extremely stupid way to install packages but I leik it

(package-initialize)
(when (not package-archive-contents)
  (package-refresh-contents))

(defvar myPackages
  '(eziam-theme
    lsp-mode
    flycheck
    company-lsp
    use-package
    lsp-ui
    dap-mode
    yasnippet
    fsharp-mode
    fstar-mode
    projectile
    clang-format
    magit
    ace-window))

(mapc #'(lambda (package)
    (unless (package-installed-p package)
      (package-install package)))
      myPackages)

;; LSP

(setq lsp-clients-clangd-executable "clangd-6.0")

(use-package lsp-mode
	     :hook (c++-mode . lsp)
	     :commands lsp
	     :config
	     (add-hook 'python-mode-hook #'lsp)
	     )


(use-package lsp-ui :commands lsp-ui-mode)
(use-package company-lsp :commands company-lsp)
(use-package lsp-treemacs :commands lsp-treemacs-errors-list)

(global-set-key (kbd "C-c i") 'clang-format-region)
(global-set-key (kbd "C-c u") 'clang-format-buffer)

(setq clang-format-style-option "Google")


(setq-default electric-indent-inhibit t)

;; Make the backspace properly erase the tab instead of
;; removing 1 space at a time.
(setq backward-delete-char-untabify-method 'hungry)

;; Magit

(global-set-key (kbd "C-x g") 'magit-status)
(global-set-key (kbd "C-x M-g") 'magit-dispatch)

;; Ace Window
(global-set-key (kbd "C-x M-g") 'magit-dispatch)



(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#FAF7EE" "#4E4B3D" "#737063" "#201D0E" "#373426" "#4E4B3D" "#292617" "#201D0E"])
 '(custom-enabled-themes (quote (eziam-dark)))
 '(custom-safe-themes
   (quote
    ("5a53a0e9035d42c86df6e305544dc9a278d437d31f0b3ede892f6feb292b018f" "4cc73e7a73a52b68d053a22ce3c732ad89b5e61e70f1f8133341c4237477cf0e" "a5f068e6c26c2ed952096c034eb49f3ad15a329c905bf3475fae63c1ddb9f402" "341b2570a9bbfc1817074e3fad96a7eff06a75d8e2362c76a2c348d0e0877f31" "801a567c87755fe65d0484cb2bded31a4c5bb24fd1fe0ed11e6c02254017acb2" "dbade2e946597b9cda3e61978b5fcc14fa3afa2d3c4391d477bdaeff8f5638c5" default)))
 '(fci-rule-color "#FFFFF8")
 '(package-selected-packages (quote (eziam-theme tao-theme)))
 '(pdf-view-midnight-colors (quote ("#ffffff" . "#444444")))
 '(vc-annotate-background "#FFFFFD")
 '(vc-annotate-color-map
   (quote
    ((20 . "#D5D2C8")
     (40 . "#B0ADA2")
     (60 . "#B0ADA2")
     (80 . "#737063")
     (100 . "#737063")
     (120 . "#4E4B3D")
     (140 . "#4E4B3D")
     (160 . "#373426")
     (180 . "#373426")
     (200 . "#373426")
     (220 . "#292617")
     (240 . "#292617")
     (260 . "#292617")
     (280 . "#201D0E")
     (300 . "#201D0E")
     (320 . "#201D0E")
     (340 . "#1B1809")
     (360 . "#1B1809"))))
 '(vc-annotate-very-old-color "#4E4B3D"))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
